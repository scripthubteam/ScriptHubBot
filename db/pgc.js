var { Client } = require('pg');
var client = new Client({
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DB,
    port: process.env.DB_PORT,
    host: process.env.DB_HOST,
    ssl: true
}); 

	client.connect();
	console.log("DB-PGC Connected.")
module.exports = client;
