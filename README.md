# ScriptHubBot 

Este es el bot que sirve al servidor de Discord de Script Hub. Actualmente hosteado en [Glitch](https://glitch.com)

## Componentes Importantes

* UpTimeRobot - Asegura a la aplicación mantenerse 24/7.
* Glitch - Servidor donde se hospedan los archivos y el backend del Node.js.
* git - Control de versión vinculado con este repositorio.
* pgc
