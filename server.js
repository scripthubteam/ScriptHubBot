
/*Script Hub - Server bot*/
// server.js
// where your node app starts

// init project
const express = require('express');
const app = express();

// we've started you off with Express, 
// but feel free to use whatever libs or frameworks you'd like through `package.json`.

// http://expressjs.com/en/starter/static-files.html
app.use(express.static('public'));

// http://expressjs.com/en/starter/basic-routing.html
app.get('/', function(request, response) {
  response.sendFile(__dirname + '/views/index.html');
});

// listen for requests :)
const listener = app.listen(process.env.PORT, function() {
  console.log('Webserver' + listener.address().port);
});

setInterval(() => {
  app.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`); 
}, 280000);
/*----------------------------------------------------------------------------------------------------------*/

const Discord = require('discord.js');
const db = require('quick.db')
const dbP = require("./db/pgc.js")
const moment = require('moment');
const bot = new Discord.Client();
const config = require("./config.json")
const fs = require("fs");
const tools = require('./functions.js')

var cmdExt;
bot.commands = new Discord.Collection();
bot.aliases = new Discord.Collection();

const log = (message) => {
  console.log(`[${moment().format("MM-DD-YYYY HH:mm:ss")}] ${message}`);
};

//Eventos
fs.readdir('./events/', (err, files) => {
  if (err) throw err;
  log(`[Eventos] [Cargando un total de ${files.length} eventos]`);

  files.forEach((f) => {
    const event = require(`./events/${f}`);
    const eventName = f.split('.')[0];
    bot.on(eventName, event.bind(null, bot));
    delete require.cache[require.resolve(`./events/${f}`)];
  });
});

//Comandos
fs.readdir("./commands/", (err, files) => {
  if (err) console.error(err);
  
  log(`[Comandos] [Cargando un total de ${files.length} comandos]`);
  files.forEach(f => {
    var FcommandName = f.replace(".js", "")
    log(`Comando cargado: ${f}.`);
    if (fs.existsSync('./commands/'+f)) try { cmdExt = require('./commands/'+f) } catch (e) { log("[ERROR] No es posible encontrar el comando recargado. Razón:\n"+e) }
      if (fs.existsSync('./commands/'+f)) {
        fs.watchFile('./commands/'+f, function (cur, prev) {
          delete require.cache[require.resolve('./commands/'+f)]
          try {
            cmdExt = require('./commands/'+f)
            log(`[${config.prefix}${FcommandName}] El comando fue actualizado.`)
          } catch (e) {
            log(`[${config.prefix}${FcommandName}] El comando fue modificado, pero no es posible actualizarlo. Razón:\n${e}`)
          }
        })
      }
    let props = require(`./commands/${f}`);
    props.help.name = FcommandName;
    props.conf.aliases.forEach(alias => {
      bot.aliases.set(alias, FcommandName);
    });
    bot.commands.set(FcommandName, props);

  });
});

/**:::::::::::::MESSAGE::::::::::::::::::**/
bot.on("message", async message => { 
const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
const command = args.shift().toLowerCase();
const timeUsed = new Set()
//hola seba como estas? Sebas: Hola Lauti <3
dbP.query("INSERT INTO discord_users(id, nick, avatar_url, created_at) VALUES($1, $2, $3, $4)", [message.author.id, message.author.tag, message.author.displayAvatarURL(), message.author.createdAt], (err, res) => {
  console.log(err ? err.stack : res.rows[0].message)
  dbP.end()
})
dbP.query("UPDATE discord_users SET nick = $1, avatar_url = $2 WHERE id = $3", [message.author.tag, message.author.displayAvatarURL(), message.author.id], (err, res) => {
  console.log(err ? err.stack : res.rows[0].message)
  dbP.end()
})

if (!message.content.startsWith(config.prefix) || message.author.bot) return;
 let cooltimer = 5000;
    if (timeUsed.has(message.author.id)){
      message.channel.send({embed: {
        color: 0xdd0000,
        fields: [{
          name: ":timer: ¡Cálmate por favor!",
          value: "Para volver a utilizar nuevamente este comando debes esperar `5 segundos`"
        }
        ],
        timestamp: new Date(),
        footer: {
          icon_url: message.author.avatarURL,
          text: message.author.username+"#"+message.author.discriminator
        }
      }
    }).then(m => {
      setTimeout(() => {
        m.delete()
      }, 5000)
    })
      return;
    }
    let cmdFile;
    let cmdData;
  try{
    cmdData = command;
    //Cooldown
    timeUsed.add(message.author.id)
    setTimeout(() => {// 2.5 segundos
      timeUsed.delete(message.author.id)
    }, cooltimer)

    if(message.content === config.prefix+cmdData+" help"){
      if(bot.commands.has(cmdData)){
        cmdData = bot.commands.get(cmdData)
        if(!cmdData.help.img){
          cmdData.help.img = "https://scripthubteam.github.io/docs/assets/server-ui/logo-cord-re.png"
        }
        message.channel.send({
          "embed": {
            "title": cmdData.help.name,
            "description": cmdData.help.description+"\n**Uso:** "+cmdData.help.usage+"\n**Apodo(s):** "+cmdData.conf.aliases,
            "color": 100554,
            "thumbnail": {
              "url": cmdData.help.img
            }
          }
        }).catch(e => {
          message.channel.send("**¡Oops! Este comando no está funcionando en este momento, intente más tarde.**")
          log("[Error] Error del CommandHelper:\n"+e)
        })
        return;
      }
      return;
    }

    //Command's Perm-Level
    if(bot.commands.has(command) || bot.aliases.has(command)){
      let thecmdData = bot.commands.get(command) || bot.commands.get(bot.aliases.get(command))

      //Administrador lvl 2
      if(thecmdData.conf.permLevel === 2 && message.member.roles.has(config.adminsRole) === false){
        return message.channel.send("Lo siento, este comando solo está disponible para miembros del rol `"+message.guild.roles.get(config.adminsRole).name+"`")
      }

      //Moderador lvl 1
      if(thecmdData.conf.permLevel === 1 && message.member.roles.has(config.modsRole) === false){
        return message.channel.send("Lo siento, este comando solo está disponible para miembros del rol `"+message.guild.roles.get(config.modsRole).name+"`")
      }

    let cmdFile = bot.commands.get(command) || bot.commands.get(bot.aliases.get(command));
    cmdFile.run(bot, message, args)
    return;
    }

  } catch (err){
  console.error(`[ERROR] El comando '${command}' falló:\n`+err.stack);
  }
});

bot.on('warn', e =>{
  console.log("WARN: "+moment().format("MM-DD-YYYY HH:mm:ss"))
  console.log(e);
});

bot.on('error', e =>{
  console.log("ERROR: "+moment().format("MM-DD-YYYY HH:mm:ss"))
  console.log(e);
});

bot.login(process.env.TOKEN)