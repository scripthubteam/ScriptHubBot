const db = require("quick.db")
module.exports = async(bot, member) => {
  
  let channelLogHub= bot.channels.get("441340173918142474")
  let dbBot = db.get("BotStorage_"+member.user.id)

if(member.user.bot){
  if(dbBot){
  let getOwner = bot.users.get(dbBot.owner.id)
  let embed = {
  "title": "¡Nuevo Bot: "+member.user.tag+"!",
  "description": "**◈ Prefix:** "+dbBot.config.prefix+"\n**◈ Owner** "+getOwner.tag,
  "color": 109494,
  "timestamp": new Date(),
  "footer": {
    "text": member.guild.name

  },
  "image": {
    "url": member.user.displayAvatarURL()
  }
};
/*BotChannelSender*/
  let channelBotPlay = bot.channels.get("440682985134620682")
  channelBotPlay.send({ embed })
//

  channelLogHub.send(":robot: [BOT] **"+member.user.username+"** fue verificado como miembro del **Club de Bots**.")

member.roles.add(member.guild.roles.find(f => f.name === "Bots")); //ROL: Bots
db.set("BotStorage_"+member.user.id+".data.appr.isAppr", true);
db.set("BotStorage_"+member.user.id+".data.appr.day", Date.now());

getOwner.send({"embed": {
    "color": 302176,
    "timestamp": new Date,
    "footer": {
      "text": "Equipo de Aprobación de Aplicaciones"
    },
    "thumbnail": {
      "url": member.user.displayAvatarURL()
    },
    "image": {
      "url": "https://i.imgur.com/D56tkxB.png"
    },
    "fields": [
      {
        "name": "<:verific:441100761771016214> ¡"+member.user.tag+" fue aprobado por el Equipo!",
        "value": "Su Bot ahora es parte de **"+member.guild.name+"** y miembro del **Club de Bots**. Visite el canal <#440682985134620682> y utilice el comando `s/infobot "+member.user.username+"` para obtener información sobre su membresía."
      }
    ]
  }
})
return;
}

channelLogHub.send(":robot: [BOT] **"+member.user.username+"** entró al servidor.")
return;
}

let embed = {
  "title": "¡Bienvenido/a "+member.user.username+"!",
  "description": "Gracias por unirte a **Script Hub**.\n**—** Lee <#440978485964701706> para empezar tu recorrido por el servidor.\n**—** Lee <#440555148054626336> para seguir el fundamento del servidor.\n**—** ¿Necesitas ayuda? Consulta tus dudas en <#441011908817059840>.",
  "color": 255068,
  "timestamp": new Date(),
  "footer": {
    "text": member.guild.name+" - N#"+member.guild.memberCount
  },
  "image": {
    "url": "https://i.imgur.com/D56tkxB.png"
  }
};
  let channelGuild = bot.channels.get("440552359542652929") //#general
  channelGuild.send(member.user,{ embed })

  //LOGHUB
channelLogHub.send("[USER] **"+member.user.username+"** entró al servidor.")
  return;
  
}