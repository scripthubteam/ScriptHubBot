const db = require("quick.db")
module.exports = async(bot, member) => {
  
//LOGHUB
  let channelBot = bot.channels.get("440682985134620682") //#playground
  let channelLogHub= bot.channels.get("441340173918142474") //#loghub
  let dbBot = db.get("BotStorage_"+member.user.id)

  if(member.user.bot){
    if(dbBot){
       let getOwner = bot.users.get(dbBot.owner.id)

      getOwner.send({"embed": {
    "color": 302176,
    "timestamp": new Date,
    "footer": {
      "text": "Equipo de Aprobación de Aplicaciones"
    },
    "image": {
      "url": "https://i.imgur.com/D56tkxB.png"
    },
    "fields": [
      {
        "name": ":x: Su Bot fue expulsado de Script Hub.",
        "value": "¿Esto es un error? Contacte a un Administrador."
      }
    ]
  }
})
      db.delete("BotStorage_"+member.user.id)
      channelBot.send(":robot: El bot **"+member.user.username+"#"+member.user.discriminator+"** no pertenece más al **Club de Bots**.")
      channelLogHub.send(":robot: [CLUB DE BOTS] **"+member.user.username+"** salió del servidor por no formar parte del **Club de Bots.**")
      return;
    }
    //
   
    channelLogHub.send(":robot: [COMÚN] **"+member.user.username+"** salió del servidor.")
    return;
  }

  channelLogHub.send("[USER] **"+member.user.username+"** salió del servidor.")
return;
  
}