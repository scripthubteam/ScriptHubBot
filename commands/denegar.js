let db = require("quick.db")
let config = require("../config.json")
exports.run = async function(bot, message, args) {
let argsData = args.join(" ")
let parts = argsData.split(";"),
                    id = parts[0],
                    reason = parts[1];
if(!id){
  message.channel.send(":x: **Debes introducir una ID.**")
  return;
}

const dbBot = await db.fetch("BotStorage_"+id)
const thebot = await bot.users.fetch(id)

if(!thebot.bot){
  message.channel.send(":x: **La ID introducida no pertenece a un Bot**.")
  return;
}

if(dbBot !== null){
if(dbBot.data.appr.isAppr === true){
  message.channel.send(":x: Este Bot **no está en fase de solicitud**, fue aprobado el día **"+dbBot.data.appr.day+"**.")
  return;
}

if(!reason){
  bot.users.fetch(dbBot.owner.id).then(a => {
      message.channel.send(":information_source: **El motivo no fue introducido, pero se utilizará uno predeterminado**")
            a.send(":information_source: Mensaje del **Equipo Administrativo de Script Hub** envíado por el encargado en **Aprobación de solicitudes de Bots** ー **"+message.author.tag+"**:\n\n**¡Hola "+a.username+"!**\nEste mensaje fue enviado para notificarte nuestra decisión sobre la solicitud dada el día **"+dbBot.data.request.day+"** para la aprobación de tu Bot en nuestros servicios:\n **Ha sido rechazada, vuelve a intertarlo otro día.** Puedes pedirle motivos al encargado para obtener más información sobre la aprobación.")
      db.delete("BotStorage_"+id[0]).then(i => {
      message.channel.send(":white_check_mark: La solicitud de **"+thebot.tag+"** fue rechazada con éxito.")
      return;
      })
          })
  return;
}

bot.users.fetch(dbBot.owner.id).then(a => {
            a.send(":information_source: Mensaje del **Equipo Administrativo de Script Hub** envíado por el encargado en **Aprobación de solicitudes de Bots** ー **"+message.author.tag+"**:\n\n**¡Hola "+a.username+"!**\nEste mensaje fue enviado para notificarte nuestra decisión sobre la solicitud dada el día **"+dbBot.data.request.day+"** para la aprobación de tu Bot en nuestros servicios:\n "+reason[1].join(" "))
            db.delete("BotStorage_"+id).then(i => {
        message.channel.send(":white_check_mark: La solicitud de **"+thebot.tag+"** fue rechazada con éxito.")
      })
            return;
          })
return;
}

message.channel.send(":x: **La ID del bot introducida no pertenece a una solicitud existente**.")
};

exports.conf = {
  categoria: "Administración",
  aliases: ["dn"],
  permLevel: 2
};

exports.help = {
  name: "denegar",
  description: "Deniega la solicitud de entrada de un Bot.",
  usage: "<ID>",
  img: "http://ktr.org.pl/wp-content/uploads/2017/09/1..gif"
};