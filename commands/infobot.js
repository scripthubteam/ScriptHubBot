const db = require("quick.db")
const dbP = require("../db/pgc.js")
const colorimage = require("img-color")
const ult = require("../functions.js")
const config = require("../config.json")
exports.run = async function(bot, message, args) {
  // Constantes para la obtención del usuario

  const mentbot = message.mentions.members.first()
  const cmd_data = args.join(" ")

// Variables
let parts = cmd_data.split(";"),
            botname = parts[0],
            action = parts[1],
            mcontent = parts[2];

  // Campo de argumentación
  if(!botname){
    message.channel.send(':x: **Es necesario que insertes el nombre del bot.** Puedes @mencionarlo, poner su nombre o ID.')
    return;
  }

  // Convertidores de usuario - @mención, username e ID.
  let botnameConverted

  if(typeof botname === "string"){
    botnameConverted = bot.users.find(f => f.username === botname)
  }

  if(isNaN(botname) === false){
    botnameConverted = bot.users.get(botname)
  }

  if(mentbot){
    botnameConverted = mentbot.user
  }

  if(!botnameConverted){
    message.channel.send(":x: El bot que intentas buscar no existe.")
    return;
  }

  if(botnameConverted.bot !== true){
    message.channel.send(":x: **Lo siento, pero la ID introducida no pertenece a un bot.** No puedes introducir la ID de un usuario común!")
    return;
  }

  // Chequeador de base de datos
  const dbBot = await db.fetch("BotStorage_"+botnameConverted.id)
  //const doColor = await colorimage.getDominantColor(botnameConverted.displayAvatarURL)
  if(dbBot){
    let getTheOwner = dbBot.owner.id
    if(bot.users.get(getTheOwner)){
      getTheOwner = bot.users.get(getTheOwner).tag
    }
    if(isNaN(getTheOwner) === false){
      getTheOwner = dbBot.owner.id+" (Abandonó el servidor)"
      bot.channels.get(config.channel.staff).send(" Se ha detectado que el usuario <@"+dbBot.owner.id+"> abandonó el servidor y su bot **"+bot.users.get(dbBot.data.id).tag+"** está en el servidor. **ESTO AMERITA UN KICK A LA APLICACIÓN**")
    }
    let imTheOwnerLol = ''
    let isCertified = ''
    if(dbBot.owner.id === message.author.id){
      imTheOwnerLol = "(Propiedad tuya)"
    }
 
    /*
    if(dbBot.config.certified){
      isCertified = "<:verific:441100761771016214>"
    }
    */

    //RichEmbed
    const embed = {"embed": {
    "description": dbBot.config.info,
    "color": message.member.roles.first().color,
    "footer": {
      "icon_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Star_icon_stylized.svg/2000px-Star_icon_stylized.svg.png"
    },
    "thumbnail": {
      "url": botnameConverted.displayAvatarURL()
    },
    "author": {
      "name": botnameConverted.tag+isCertified+" "+imTheOwnerLol,
      "icon_url": botnameConverted.displayAvatarURL()
    },
    "fields": [
      {
        "name": "ℹ Prefix",
        "value": "`"+dbBot.config.prefix+"`"
      },
      
      {
        "name": "💻 Desarrollador",
        "value": getTheOwner,
        "inline": true
      }
    ]
  }
}

    /*Sección de acciones*/

    //Descripción
    if(action == "desc"){
    	if(dbBot.owner.id === message.author.id || message.member.roles.has(config.adminsRole) === true || message.member.roles.has(config.modsRole) === true){
    		if(!mcontent){
    			message.channel.send(":x: Debes agregar un mensaje.")
    			return;
    		}
    		if(mcontent.length > 30){
    			mcontent = mcontent.substring(0, 30)
    		}
    		db.set("BotStorage_"+botnameConverted.id+".config.info", mcontent)
        dbP.query("INSERT INTO bots(info) VALUES($1)", [mcontent], (err, res) => {
        console.log(err ? err.stack : res.rows[0].message) 
        dbP.end()
      })
    		message.channel.send(":white_check_mark: **Descripción actualizada.**")
    		message.channel.send("```"+mcontent+"```")
    		if(message.member.roles.has(config.adminsRole) === true || message.member.roles.has(config.modsRole) === true && message.author.id !== dbBot.owner.id){
    			bot.users.get(dbBot.owner.id).send(":information_source: La descripción de su bot **"+bot.users.get(dbBot.data.id).tag+"** fue cambiada por un miembro del equipo de **Script Hub** a `"+mcontent+"`.")
    		}
    		return;
    	}
    	message.channel.send(":x: Para cambiar la descripción de este bot **es necesario que tú seas el dueño.**")
    	return;
    }

//Prefijo
    if(action === "prefix"){
    	if(dbBot.owner.id === message.author.id || message.member.roles.has(config.adminsRole) === true || message.member.roles.has(config.modsRole) === true){
    		if(!mcontent){
    			message.channel.send(":x: Debes agregar el prefijo que quieres añadir.")
    			return;
    		}
    		if(mcontent === dbBot.config.prefix){
    			message.channel.send(":x: ¿De nuevo el mismo prefijo?")
    			return;
    		}
    		if(mcontent.length > 5){
    			message.channel.send(":x: Prefijo demasiado largo.")
    			return;
    		}
    		db.set("BotStorage_"+botnameConverted.id+".config.prefix", mcontent)
        dbP.query("INSERT INTO bots(prefix) VALUES($1)", [mcontent], (err, res) => {
        console.log(err ? err.stack : res.rows[0].message) 
        dbP.end()
      })
    		message.channel.send(":white_check_mark: **Prefijo actualizado.**")
    		message.channel.send("```"+dbBot.config.prefix+" --> "+mcontent+" (nuevo)```")
    		if(message.member.roles.has(config.adminsRole) === true || message.member.roles.has(config.modsRole) === true && message.author.id !== dbBot.owner.id){
    			bot.users.get(dbBot.owner.id).send(":information_source: El prefijo de su bot **"+bot.users.get(dbBot.data.id).tag+"** fue cambiada por un miembro del equipo de **Script Hub** a `"+mcontent+"`.")
    		}
    		return;
    	}
    	message.channel.send(":x: Para cambiar el prefijo de este bot **es necesario que tú seas el dueño.**")
    	return;
    }

    //Rating
    if(action === "vote"){
      if(dbBot.config.votes_plus === undefined){
        db.set("BotStorage_"+botnameConverted.id+".config.votes_plus", 0)
        db.set("BotStorage_"+botnameConverted.id+".config.votes_negative", 0)
        db.set("BotStorage_"+botnameConverted.id+".config.votes_players", [])
        message.channel.send("Hemos actualizado algunas configuraciones de este bot, si usaste un comando por favor intentalo de nuevo.")
        return;
      }
      for(let x in dbBot.config.votes_players){
        if(dbBot.config.votes_players[x] === message.author.id){
          message.channel.send(":x: Lo siento, pero solo se puede votar una vez.")
          return;
        }
        return;
      }
        if(!mcontent){
          message.channel.send(":x: Debes votar de la siguiente manera: \n ```\n Para votar positivamente:\n --- s/infobot "+botnameConverted.username+";vote;up \n Para votar negativamente:\n --- s/infobot "+botnameConverted.username+";vote;down ```.")
          return;
        }
      if(mcontent === "up"){
        db.add("BotStorage_"+botnameConverted.id+".config.votes_plus", 1)
        db.add("BotStorage_"+botnameConverted.id+".config.votes_players", message.author.id)
        message.channel.send(":thumbsup: Genial, has votado **positivamente** para este bot.")
        return;
      }
      if(mcontent === "down"){
        db.add("BotStorage_"+botnameConverted.id+".config.votes_negative", 1)
        db.add("BotStorage_"+botnameConverted.id+".config.votes_players", message.author.id)
        message.channel.send(":thumbsdown: Vaya, has votado **negativamente** para este bot.")
        return;
      }
      return;
    }
    message.channel.send(embed)
    return;
  }
  return message.channel.send(":x: El bot **"+botnameConverted.username+"** no está registrado en el **Club de Bots**!")
};

exports.conf = {
  categoria: "Información",
  aliases: ["ib"],
  permLevel: 0
};

exports.help = {
  name: "infobot",
  description: "Obtén la información de un bot miembro del Club de Bots.",
  usage: "<username/mención/id>;<? acción (desc, prefix)>",
  img: "https://media.giphy.com/media/5yLgoctdubLO8XGodOM/giphy.gif"
};