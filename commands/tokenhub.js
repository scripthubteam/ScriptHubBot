const config = require("../config.json")
const dbP = require("../db/pgc.js")
exports.run = async function(bot, message, args) {
  var tokenGenerator = (message.author.id*50)-message.author.id+1945;
  message.author.send("Su token único de miembro de **Script Hub** generado es: `"+tokenGenerator+"`");
  dbP.query("UPDATE discord_users SET nick = $1, avatar_url = $2 WHERE id = $3", [message.author.tag, message.author.displayAvatarURL(), message.author.id], (err, res) => {
    console.log(err ? err.stack : res.rows[0].message)
    dbP.end()
    return;
  })
   dbP.query("INSERT INTO tmp_registration(hash_code, fk_discord_users) VALUES($1, $2)", [tokenGenerator, message.author.id], (err, res) => {
    console.log(err ? err.stack : res.rows[0].message)
    dbP.end()
  })
      
    return;
};

exports.conf = {
  categoria: "General",
  aliases: ["thub"],
  permLevel: 0
};

exports.help = {
  name: "tokenhub",
  description: "Genera tu código privado de miembro único.",
  usage: "",
  img: "https://media.giphy.com/media/ytwDCGipNV2sTbndUA/giphy.gif"
};