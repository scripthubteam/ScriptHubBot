exports.run = async function(bot, message, args) {
  const config = require("../config.json")
  const db = require("quick.db")
  const utility = require("../functions.js")
  const hastebin = require("hastebin-gen")
  const util = require("util")
  const content = args.join(" ")
  if (content.length === 1) return
    var theeval = content;
    var lastCode = null;

    //Eval Normal
    try {
        theeval = eval(content);
        var tipo
      if (typeof theeval === 'object') {
        tipo = `[Object] - ${theeval.constructor.name}`
      } else if (typeof theeval === 'function') {
        tipo = `
          function
          ${theeval.name || theeval.length ? '-' : ''}
          ${theeval.name ? `Nombre: ${theeval.name}` : ''}
          ${theeval.name && theeval.length ? `|` : ''}
          ${theeval.length ? `Argumentos: ${theeval.length}` : ''}
        `
      } else {
        tipo = typeof theeval
      }
      if (typeof (theeval) !== 'string') {
        theeval = util.inspect(theeval, {
          showHidden: true,
          compact: false,
          depth: 0
        })
      }
      /// If 1024
       if (theeval.length > 1024 || content.length > 1024) {
        hastebin(`Salida: \n\n${theeval}`, "js").then(r => {
        message.channel.send(":link: **El resultado supera los 1024 caracteres.** Optamos por una mejor manera: "+r)
        lastCode = r;
      }).catch(e=>message.channel.send(":x: **No se pudo generar el enlace de Hastebin.** Pese a que el resultado era muy largo, se intentó generar un enlace Hastebin, pero este falló. \n"+e))
      return;
      } 
      // Normal result
    message.channel.send({embed: {
    color: 13172590,
    
    title: "Consola",
    fields: [{
        name: "INPUT",
        value: "```js\n"+content+"```"
      },
      {
        name: "OUTPUT",
        value: "```xl\n"+theeval.toString()+"```"
      },
      {
        name: "SCHEMA",
        value: "```js\n"+tipo+"```"
      }
    ]
  }
}).catch(e => message.channel.send(":x: **Hubo un error al intentar enviar el mensaje:**\n"+e))
    lastCode = {embed: {
    color: 13172590,
    
    title: "Consola",
    fields: [{
        name: "INPUT",
        value: "```js\n"+content+"```"
      },
      {
        name: "OUTPUT",
        value: "```xl\n"+theeval.toString()+"```"
      },
      {
        name: "SCHEMA",
        value: "```js\n"+tipo+"```"
      }
    ]
  }
}
    return;
      } catch(err) { 
        //Error
    message.channel.send({embed: {
    color: 13172590,
    
    title: "Consola",
    fields: [{
        name: "⚠ OUTPUT ERROR",
        value: "```xl\n"+err+"```"
      }
    ]
  }
})
      }

return;
};

exports.conf = {
  categoria: "Administración",
  aliases: ["chk"],
  permLevel: 2
};

exports.help = {
  name: "eval",
  description: "Obtén el resultado de una <evaluación>",
  usage: "<evaluación>",
  img: "https://media.giphy.com/media/ytwDCq9aT3cgEyyYVO/giphy.gif"
};