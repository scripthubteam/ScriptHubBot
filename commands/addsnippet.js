const db = require("quick.db")
const ult = require("../functions.js")
const config = require("../config.json")
exports.run = async function(bot, message, args) {
//Variables iniciales
let initCmd = args.join(" ")
let parts = initCmd.split(";"),
            $name_snp = parts[0],
            $name_snp_lang = parts[1],
            $name_snp_content = parts[2];

let $snp_db = await db.fetch("Snippet_"+$name_snp)

// Procesamiento, seguridad & validación

if(!$name_snp) return message.channel.send(":x: **Por favor, introduce un nombre a tu Snippet.** (`s/addsnippet <nombre>;<lenguaje>;<contenido>`)")
if(!$name_snp_lang) return message.channel.send(":x: **Por favor, introduce un lenguaje a tu Snippet** (`s/addsnippet "+$name_snp+";<lenguaje>;<contenido>`)")
if($name_snp_lang === "note" || $name_snp_lang === "nota"){
	$name_snp_lang = "asciidoc"
}
if(!$name_snp_content) return message.channel.send(":x: **Por favor, introduce el contenido de tu Snippet** (`s/addsnippet "+$name_snp+";"+$name_snp_lang+";<contenido>`)")	

if(args.join(" ").indexOf("@everyone") !== -1 || args.join(" ").indexOf("@here") !== -1) return message.channel.send(":x: Insertaste un caracter no permitido, por favor, corrige el contenido de tu snippet.")

$name_snp = $name_snp.replace(/\s/g,'')
$name_snp_content = $name_snp_content.replace(/```/g,'')

// Base de Datos
db.set("Snippet_"+$name_snp, {
data: {
	author: message.author.id,
	author_name: message.author.username,
	name: "Snippet_"+$name_snp,
	id: ult.genID(),
	lang: $name_snp_lang,
	dayCreate: Date.now(),
	lastMod: 0
},
content: {
	snp: $name_snp_content
},
analytic: {
	upvotes: [],
	downvotes: [],
	views: 0,
	verified: false
}
})

message.channel.send(":white_check_mark: **El Snippet "+$name_snp+" fue creado con éxito!** Usa: `s/snippet "+$name_snp+"`")

};

exports.conf = {
  categoria: "Información",
  aliases: ["addsnp"],
  permLevel: 0
};

exports.help = {
  name: "addsnippet",
  description: "¡Crea un snippet! Debes aportar el <contenido> seguido del <nombre>.",
  usage: "<nombre>;<contenido>",
  img: "https://media.giphy.com/media/5yLgoctdubLO8XGodOM/giphy.gif"
};