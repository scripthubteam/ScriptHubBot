const fs = require("fs")
const config = require("../config.json")
exports.run = async (bot, message, args) => {

    if (!args[0]) {
      
      //Categorias
      let infoCmd = ""; //Información
      let genCmd = ""; //General
      let modCmd = ""; //Moderador
      let adminCmd = ""; //Admin

      bot.commands.map(c => {
      if(c.help.usage === undefined){
        c.help.usage = "";
      }
      if(c.conf.categoria === "General"){
        genCmd += `${config.prefix}${c.help.name} ${c.help.usage}:: ${c.help.description}\n`
      }
      if(c.conf.categoria === "Información"){
        infoCmd += `${config.prefix}${c.help.name} ${c.help.usage}:: ${c.help.description}\n`
      }
      if(message.member.roles.has(config.modsRole) && c.conf.categoria === "Moderación" || !message.member.hasPermission("KICK_MEMBERS") || !message.member.hasPermission("BAN_MEMBERS")){
        modCmd = "\n= ZONA BUROCRÁTICA =\n[ Moderación ]\n"
        modCmd += `${config.prefix}${c.help.name} ${c.help.usage}:: ${c.help.description}\n`
      }
      if(message.member.roles.has(config.adminsRole) === true && c.conf.permLevel === 2 && c.conf.categoria === "Administración"){
        adminCmd = "\n[ Administración ]\n"
        adminCmd += `${config.prefix}${c.help.name} ${c.help.usage}:: ${c.help.description}\n`
      }
    })
       
      message.author.send(`Para más información sobre un comando, utilice ${config.prefix}<comando> help]\n\n[ General ]\n${genCmd}\n[ Información ]\n${infoCmd}\n${modCmd}\n${adminCmd}`,{code:"asciidoc"} );
  } 
  return;
};

exports.conf = {
  categoria: "General",
  aliases: ["h"],
  permLevel: 0
};

exports.help = {
  name : "help",
  description: "Obtén la lista de comandos de Takane.",
  usage: "<comando>",
  img: ""
};