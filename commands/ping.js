exports.run = async function(bot, message) {
	message.channel.send(':satellite:').then(sent => {
    var dataStatus;
    if(message.createdTimestamp < 200){
      dataStatus = "Estoy sufriendo..."
    }
    if(message.createdTimestamp > 200){
      dataStatus = "¡Estoy bien!"
    }
	    	sent.edit(`:sparkles:! \`${sent.createdTimestamp - message.createdTimestamp}ms\` - ${dataStatus}`)
      });
  return;
};

exports.conf = {
  categoria: "Información",
  aliases: ["pi"],
  permLevel: 0
};

exports.help = {
  name: "ping",
  description: "Un comando clásico. Me pregunto qué hace esto? *sarcasmo*",
  usage: "",
  img: "https://mir-s3-cdn-cf.behance.net/project_modules/disp/70b4b024391907.563336a912420.gif"
};