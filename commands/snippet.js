const db = require("quick.db")
const ult = require("../functions.js")
const config = require("../config.json")
exports.run = async function(bot, message, args) {
//Variables iniciales
let initCmd = args.join(" ")
let parts = initCmd.split(";"),
            $the_snp = parts[0],
            $action_snp = parts[1]

let $snp_db = await db.fetch("Snippet_"+$the_snp)                
let anexVerified = ""
let anexLastMod = ""

if(!$the_snp) return message.channel.send(":x: **Introduzca el nombre del Snippet**")

//if($action_snp === "")

if($snp_db){
  //DB Variables
  let autor = bot.users.get($snp_db.data.author),
                    id = $snp_db.data.id,
                    content = $snp_db.content.snp,
                    upvotes = $snp_db.analytic.upvotes.length,
                    downvotes = $snp_db.analytic.downvotes.length,
                    isVerified = $snp_db.analytic.verified,
                    views = $snp_db.analytic.views

if($snp_db.data.lastMod !== 0){
 anexLastMod = "\n**Última modificación** `"+new Date($snp_db.data.lastMod)+"`"
}
if(isVerified === true){
 anexVerified = "<:verific:441100761771016214> *Este es un Snippet verificado*"
}

//Views counter lol
db.add("Snippet_"+$the_snp, 1, {target: ".analytic.views"})

message.channel.send("**Autor:** `"+(typeof autor !== undefined ? autor.tag : $snp_db.data.author_name)+"`\n**Creado el:** `"+new Date($snp_db.data.dayCreate)+"`"+anexLastMod+"\n**Vistas:** `"+views+"`")
message.channel.send(""+anexVerified+"\n```"+$snp_db.data.lang+"\n"+$snp_db.content.snp+"```")
return;
}
message.channel.send(":x: **El Snippet `"+$the_snp+"` no existe.** Utiliza el comando `s/listsnippet` para ver el listado de Snippets.")
return;
};

exports.conf = {
  categoria: "Información",
  aliases: ["snp"],
  permLevel: 0
};

exports.help = {
  name: "snippet",
  description: "Obtén el cotenido de un snippet.",
  usage: "<nombre>",
  img: "https://media.giphy.com/media/5yLgoctdubLO8XGodOM/giphy.gif"
};