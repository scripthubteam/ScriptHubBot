const db = require("quick.db")
const dbP = require("../db/pgc.js")
const config = require("../config.json")
const tools = require("../functions.js")
exports.run = async function(bot, message, args) {
if (message.channel.id !== config.channel.botinvite) {
  return;
}
    if (isNaN(args[0])) {
      tools.embed(message.channel, ':x: **Esa no es una ID válida.** La ID debe contener el número del cliente de la apliación/usuario.', 10000)
      return;
    }
    //Warning: Asuka smug
    if(args[0] === bot.user.id){
      message.channel.send({
        content: "Sí, claro.",
        files: ["https://i.kym-cdn.com/photos/images/original/001/044/374/2f0.png"]
      })
      return;
    }
    if (!args[1]) {
      tools.embed(message.channel, ':x: **Por favor, especifica un prefix para tu bot.** El prefix ayudará a saber cómo se ejecuta tu bot.', 10000)
      return;
    }
    const user = await bot.users.fetch(args[0])
    const dbBot = await db.fetch("BotStorage_"+args[0])

      if(!user.bot){
        message.channel.send(":x: **La ID introducida no pertenece a un Bot**.")
        return;
      }
       if(dbBot !== null){
        if(dbBot.data.appr.isAppr === true && dbBot.owner.id === message.author.id){
         message.channel.send(":x: **Este bot ya está aprobado.** Y es de tu propiedad, ¿sucede algo?");
         return; 
        }
        if(dbBot.data.appr.isAppr === true){
        message.channel.send(":x: Este bot ya está registrado en nuestro sistema por otro usuario. **Si esto es un error, notifiquele a un Administrador**.")
        return;
      }
    }
      db.set('BotStorage_'+user.id, { 
        data: {
          id: user.id,
          request: {
            day: Date.now()
          }, 
          appr: {
            isAppr: false,
            day: 0
          }
        },
        owner: {
          id: message.author.id
        },
        config: {
          prefix: args[1],
          info: "Beep boop beep?",
          certified: false,
          votes_plus: 0,
          votes_negative: 0,
          vote_players: []
        }
      })   

      dbP.query("INSERT INTO discord_users(id, nick, created_at, avatar_url) VALUES($1, $2, $3, $4)", [message.author.id, message.author.tag, message.author.createdAt, message.author.displayAvatarURL()], (err, res) => {
        console.log(err ? err.stack : res.rows[0].message) 
        dbP.end()
      })
      dbP.query("INSERT INTO bots(id, nick, created_at, avatar_url, owner_id, prefix, info, certified) VALUES($1, $2, $3, $4, $5, $6, $7, $8)", [user.id, user.tag, Date.now(), user.displayAvatarURL(), message.author.id, args[1], "Beep boop beep?", false], (err, res) => {
        console.log(err ? err.stack : res.rows[0].message) 
        dbP.end()
      })

      const embed_dm = {"embed": {
    "color": message.member.roles.first().color,
    "timestamp": new Date,
    "footer": {
      "text": "Equipo de Aprobación de Aplicaciones"
    },
    "image": {
      "url": "https://i.imgur.com/D56tkxB.png"
    },
    "fields": [
      {
        "name": "Su solicitud está pendiente de aprobación.",
        "value": "Le notificamos que su solicitud fue hecha el día **"+new Date+"** y está pendiente de aprobarse o ser rechazada en las próximas horas. ¡Sea paciente!"
      }
    ]
  }
}

      const embed_staffinv = { 
    "content": ":information_source: Para rechazar una solicitud, utilice `"+config.prefix+"denegar <ID>`",
    "embed": {
    "title": "Solicitud de invitación",
    "description": "El usuario **"+message.author.tag+"** ha solicitado que su bot **"+user.tag+"** sea invitado a **Script Hub**",
    "color": message.member.roles.first().color,
    "timestamp": new Date(),
    "footer": {
      "icon_url": message.author.displayAvatarURL,
      "text": "Detalles de la petición"
    }, 
    "thumbnail": {
      "url": user.displayAvatarURL()
    },
    "fields": [
      {
        "name": "🤖 Bot",
        "value": user.tag+ " [INVITAR](https://discordapp.com/api/oauth2/authorize?client_id="+args[0]+"&permissions=104187968&scope=bot) - `ID: "+user.id+"`",
        "inline": true
      },
      {
        "name": "ℹ Prefix",
        "value": "`"+args[1]+"`",
        "inline": true
      },
      {
        "name": "💻 Desarrollador",
        "value": message.author.tag+" `(Cuenta creada: "+new Date(message.author.createdTimestamp)+")`",
        "inline": true
      }
    ]
  }
}
      const embed_botinv = {"embed": {
    "title": "Solicitud de invitación",
    "description": "¡Hola **"+message.author.tag+"**! Gracias por invitar tu bot a **Script Hub**, este será en lo más breve posible introducido al servidor si cumple los requisitos. Te enviaremos una notificación cuando se decida sobre tu solicitud.",
    "color": message.member.roles.first().color,
    "timestamp": new Date(),
    "footer": {
      "icon_url": message.author.displayAvatarURL,
      "text": "Petición generada"
    }, 
    "thumbnail": {
      "url": user.displayAvatarURL()
    },
    "fields": [
      {
        "name": "🤖 Bot",
        "value": user.tag,
        "inline": true
      },
      {
        "name": "ℹ Prefix",
        "value": "`"+args[1]+"`",
        "inline": true
      },
      {
        "name": "💻 Desarrollador",
        "value": message.author.tag,
        "inline": true
      }
    ]
  }
}
  
      bot.channels.get(config.channel.botinvite).send(embed_botinv)
      message.author.send(embed_dm)
      bot.channels.get(config.channel.botrequest).send(embed_staffinv)

  return;
};

exports.conf = {
  categoria: "General",
  aliases: ["inv"],
  permLevel: 0
};

exports.help = {
  name: "invite",
  description: "Envia una petición de invitación para tu bot en Script Hub.",
  usage: "<ID> <prefix>",
  img: "https://media.giphy.com/media/ytwDCGipNV2sTbndUA/giphy.gif"
};