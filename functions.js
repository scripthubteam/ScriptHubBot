
module.exports = { 
  
  embed: function(channel, message, timer) { 
                                            
    channel = channel.channel || channel; 
    
    channel.send({embed:{ 
      description: message, 
      color: 0x1db954 
    }}).then(msg => { 
      if (!isNaN(timer)) msg.delete({timeout: timer}); 
    });
    
  },

 hexParaRgb: function(hex) {
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
        rgb: parseInt(result[1], 16)+parseInt(result[2], 16)+parseInt(result[3], 16)
    } : null;
}

}